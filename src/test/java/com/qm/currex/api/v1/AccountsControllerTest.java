package com.qm.currex.api.v1;

import com.qm.currex.bussiness.model.Account;
import com.qm.currex.core.AccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

public class AccountsControllerTest {

    private AccountsController accountsController;
    private AccountService accountService;

    @BeforeEach
    public void beforeEach() {
        accountService = Mockito.mock(AccountService.class);
        accountsController = new AccountsController(accountService);

        List<Account> accounts = new ArrayList<>();
        Account account = new Account();
        account.setId(UUID.fromString("2a1dba81-4999-4e9a-80c3-55cf3ca7c364"));
        account.setBalance(BigDecimal.valueOf(1000.0));
        accounts.add(account);

        account = new Account();
        account.setId(UUID.fromString("6227f4b7-a336-4ea6-a6ec-894e1fefc947"));
        account.setBalance(BigDecimal.valueOf(2500.0));
        accounts.add(account);

        when(accountService.getAccounts()).thenReturn(accounts);
    }

    @Test
    public void shouldReturnAllAccounts() {
        List<Account> accountsResult = accountsController.getAllAccounts();

        assertNotNull(accountsResult);
        assertTrue(accountsResult.size() == 2);
        assertEquals(UUID.fromString("2a1dba81-4999-4e9a-80c3-55cf3ca7c364"), accountsResult.get(0).getId());
        assertEquals(BigDecimal.valueOf(1000.0).setScale(2), accountsResult.get(0).getBalance());

        assertEquals(UUID.fromString("6227f4b7-a336-4ea6-a6ec-894e1fefc947"), accountsResult.get(1).getId());
        assertEquals(BigDecimal.valueOf(2500.0).setScale(2), accountsResult.get(1).getBalance());
    }

}
