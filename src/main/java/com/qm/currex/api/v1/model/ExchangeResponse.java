package com.qm.currex.api.v1.model;

import java.math.BigDecimal;
import java.time.LocalDate;

public class ExchangeResponse {

    private BigDecimal moneyAmount;
    private String currency;
    private double exchangeRate;
    private LocalDate date;

    public ExchangeResponse(BigDecimal moneyAmount, String currency, double exchangeRate, LocalDate date) {
        this.moneyAmount = moneyAmount;
        this.currency = currency;
        this.exchangeRate = exchangeRate;
        this.date = date;
    }

    public BigDecimal getMoneyAmount() {
        return moneyAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }

    public LocalDate getDate() {
        return date;
    }

}
