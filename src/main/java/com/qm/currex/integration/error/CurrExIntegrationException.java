package com.qm.currex.integration.error;

public class CurrExIntegrationException extends Exception {

    private String errorCode;

    public CurrExIntegrationException(final String message, final String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

}
