package com.qm.currex.api.v1;

import com.qm.currex.bussiness.model.Account;
import com.qm.currex.core.AccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
class AccountsControllerIntegrationTest {

    @InjectMocks
    private AccountsController accountsController;

    @Mock
    private AccountService accountService;

    @BeforeEach
    void beforeEach() {
        List<Account> accounts = new ArrayList<>();
        Account account = new Account();
        account.setId(UUID.fromString("2a1dba81-4999-4e9a-80c3-55cf3ca7c364"));
        account.setBalance(BigDecimal.valueOf(1000.0));
        accounts.add(account);

        account = new Account();
        account.setId(UUID.fromString("6227f4b7-a336-4ea6-a6ec-894e1fefc947"));
        account.setBalance(BigDecimal.valueOf(2500.0));
        accounts.add(account);

        when(accountService.getAccounts()).thenReturn(accounts);
    }

    @Test
    public void shouldReturnAllAccounts() {
        List<Account> accounts = accountsController.getAllAccounts();
        assertNotNull(accounts);
        assertTrue(accounts.size() == 2);
        assertEquals(UUID.fromString("2a1dba81-4999-4e9a-80c3-55cf3ca7c364"), accounts.get(0).getId());
        assertEquals(BigDecimal.valueOf(1000.0).setScale(2), accounts.get(0).getBalance());

        assertEquals(UUID.fromString("6227f4b7-a336-4ea6-a6ec-894e1fefc947"), accounts.get(1).getId());
        assertEquals(BigDecimal.valueOf(2500.0).setScale(2), accounts.get(1).getBalance());
    }

}