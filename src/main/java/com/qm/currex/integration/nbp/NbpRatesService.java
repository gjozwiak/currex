package com.qm.currex.integration.nbp;

import com.qm.currex.integration.error.CurrExIntegrationException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "nbpRateService", url = "${integration.nbp.rates.url}")
public interface NbpRatesService {

    @GetMapping(value = "/A/{currencyCode}/{date:[0-9]{4}-[0-9]{2}-[0-9]{2}}")
    NbpRate getRate(@PathVariable("currencyCode") final String currencyCode,
                    @PathVariable("date") final String date) throws CurrExIntegrationException;

}
