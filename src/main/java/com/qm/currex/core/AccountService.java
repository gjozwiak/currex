package com.qm.currex.core;

import com.qm.currex.bussiness.model.Account;

import java.util.List;

public interface AccountService {

    List<Account> getAccounts();

}
