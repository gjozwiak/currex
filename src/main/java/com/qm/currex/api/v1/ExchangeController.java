package com.qm.currex.api.v1;

import com.qm.currex.api.v1.model.ExchangeResponse;
import com.qm.currex.bussiness.model.ExchangeResult;
import com.qm.currex.core.ExchangeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/exchange")
public class ExchangeController {

    private static final String DEFAULT_CURRENCY_CODE = "EUR";
    private static final LocalDate DEFAULT_RATE_DATE = LocalDate.parse("2022-02-11", DateTimeFormatter.ISO_LOCAL_DATE);

    private ExchangeService exchangeService;

    public ExchangeController(ExchangeService exchangeService) {
        this.exchangeService = exchangeService;
    }

    @GetMapping(value = "/{clientId:[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}}")
    public ExchangeResponse exchangeToEuro(@PathVariable final UUID clientId) {

        final ExchangeResult exchangeResult =
            exchangeService.foreignCurrencyAccountBalance(clientId, DEFAULT_CURRENCY_CODE, DEFAULT_RATE_DATE);

        return new ExchangeResponse(exchangeResult.getMoneyValue(), DEFAULT_CURRENCY_CODE, exchangeResult.getRate(),
            exchangeResult.getDate());
    }

}
