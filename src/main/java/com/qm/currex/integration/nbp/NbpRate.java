package com.qm.currex.integration.nbp;

import java.util.List;
import java.util.Map;

public class NbpRate {

    private String table;
    private String currency;
    private String code;
    private List<Map<String, Object>> rates;

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Map<String, Object>> getRates() {
        return rates;
    }

    public void setRates(List<Map<String, Object>> rates) {
        this.rates = rates;
    }

}
