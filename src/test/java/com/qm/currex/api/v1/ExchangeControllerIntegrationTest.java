package com.qm.currex.api.v1;

import com.qm.currex.bussiness.model.ExchangeResult;
import com.qm.currex.core.ExchangeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ExchangeController.class)
class ExchangeControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ExchangeService exchangeService;

    @Test
    void exchangeToEuro_200() throws Exception {

        when(exchangeService.foreignCurrencyAccountBalance(any(UUID.class), any(), any())).thenReturn(
            new ExchangeResult());
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/exchange/321e7f98-72b7-409e-9a52-24bdcc58d74c"))
            .andExpect(status().isOk());

    }

    @Test
    void exchangeToEuro_404() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/exchange/wrong-path"))
            .andExpect(status().isNotFound());
    }

}