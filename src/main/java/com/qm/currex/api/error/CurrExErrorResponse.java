package com.qm.currex.api.error;

public class CurrExErrorResponse {

    private String message;
    private String errorCode;

    public CurrExErrorResponse(String message, String errorCode) {
        this.message = message;
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public String getErrorCode() {
        return errorCode;
    }

}