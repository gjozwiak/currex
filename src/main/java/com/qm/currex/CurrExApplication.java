package com.qm.currex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class CurrExApplication {

    public static void main(String[] args) {
        SpringApplication.run(CurrExApplication.class, args);
    }

}
