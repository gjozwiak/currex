package com.qm.currex.core;

import com.qm.currex.bussiness.model.ExchangeResult;

import java.time.LocalDate;
import java.util.UUID;

public interface ExchangeService {

    ExchangeResult foreignCurrencyAccountBalance(UUID clientId, String currencyCode, LocalDate date);

}
