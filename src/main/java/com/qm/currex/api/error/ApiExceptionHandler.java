package com.qm.currex.api.error;

import com.qm.currex.bussiness.error.CurrExException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(ApiExceptionHandler.class);

    @ExceptionHandler(value = {CurrExException.class})
    public ResponseEntity<CurrExErrorResponse> handleConflict(CurrExException ex) {
        logger.error(ex.getMessage());
        final CurrExErrorResponse errorResponse = new CurrExErrorResponse(ex.getMessage(), ex.getErrorCode());
        return new ResponseEntity(errorResponse, new HttpHeaders(), UNPROCESSABLE_ENTITY);
    }

}