package com.qm.currex.api.error;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@RestController
public class CurrExErrorController implements ErrorController {

    private final Logger logger = LoggerFactory.getLogger(CurrExErrorController.class);

    @Autowired
    private ErrorAttributes errorAttributes;

    @RequestMapping(value = "/error")
    public Map<String, Object> error(final HttpServletRequest request,
                                     final WebRequest webRequest,
                                     final HttpServletResponse response) {

        logger.error("Internal error occurred");
        return this.errorAttributes.getErrorAttributes(new ServletWebRequest(request),
            ErrorAttributeOptions.defaults());
    }

}
