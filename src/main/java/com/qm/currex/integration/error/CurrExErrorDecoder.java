package com.qm.currex.integration.error;

import feign.Response;
import feign.codec.ErrorDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CurrExErrorDecoder implements ErrorDecoder {

    private final Logger logger = LoggerFactory.getLogger(CurrExErrorDecoder.class);

    @Override
    public Exception decode(String s, Response response) {
        logger.error("Integration error in {}. Status: {}", s, response.status());
        return new CurrExIntegrationException("Integration error occurred", "EX-5000");
    }

}
