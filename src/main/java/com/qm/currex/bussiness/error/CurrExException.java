package com.qm.currex.bussiness.error;

public class CurrExException extends RuntimeException {

    private String errorCode;

    public CurrExException(final String message, final String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

}
