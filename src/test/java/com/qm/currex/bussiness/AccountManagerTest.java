package com.qm.currex.bussiness;

import com.qm.currex.bussiness.error.CurrExException;
import com.qm.currex.bussiness.model.Account;
import com.qm.currex.bussiness.model.ExchangeResult;
import com.qm.currex.bussiness.repository.AccountsRepository;
import com.qm.currex.integration.error.CurrExIntegrationException;
import com.qm.currex.integration.nbp.NbpRate;
import com.qm.currex.integration.nbp.NbpRatesService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.convert.converter.Converter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AccountManagerTest {

    private static final UUID NOT_EXISTED_ACCOUNT_ID = UUID.fromString("c230bfb8-154a-4d4d-85cb-129bfa30744f");
    private static final UUID EXISTED_ACCOUNT_ID = UUID.fromString("dabae540-d3db-4697-bb91-8bf728066577");
    private NbpRatesService nbpRatesService;
    private AccountsRepository accountsRepository;
    private Converter<NbpRate, ExchangeResult> nbpRateConverter;

    private AccountManager accountManager;

    @BeforeEach
    public void beforeEach() {

        prepareNbpRatesService();
        prepareAccountsRepository();
        prepareConverter();

        accountManager = new AccountManager(nbpRatesService, accountsRepository, nbpRateConverter);
    }

    @Test
    void foreignCurrencyAccountBalance() {
    }

    @Test
    public void shouldReturnAllAccounts() {
        List<Account> accountsResult = accountManager.getAccounts();

        assertNotNull(accountsResult);
        assertTrue(accountsResult.size() == 2);
        assertEquals(EXISTED_ACCOUNT_ID, accountsResult.get(0).getId());
        assertEquals(BigDecimal.valueOf(500.0).setScale(2), accountsResult.get(0).getBalance());

        assertEquals(UUID.fromString("6227f4b7-a336-4ea6-a6ec-894e1fefc947"), accountsResult.get(1).getId());
        assertEquals(BigDecimal.valueOf(2500.0).setScale(2), accountsResult.get(1).getBalance());
    }

    @Test
    public void shouldReturnCorrectExchangeCalculation() {
        ExchangeResult exchangeResult =
            accountManager.foreignCurrencyAccountBalance(EXISTED_ACCOUNT_ID, "EUR",
                LocalDate.parse("2022-02-11", DateTimeFormatter.ISO_LOCAL_DATE));
        assertEquals(BigDecimal.valueOf(110.71), exchangeResult.getMoneyValue());
    }

    @Test
    public void shouldThrowCurrExExceptionIfNoAccountFound() {
        CurrExException exception = assertThrows(
            CurrExException.class, () -> {
                accountManager.foreignCurrencyAccountBalance(NOT_EXISTED_ACCOUNT_ID, "EUR", LocalDate.now());
            });

        assertEquals("Account not found", exception.getMessage());
        assertEquals("EX-4000", exception.getErrorCode());
    }

    @Test
    public void shouldThrowCurrExExceptionIfNbpIntegrationFails() {
        CurrExException exception = assertThrows(
            CurrExException.class, () -> {
                accountManager.foreignCurrencyAccountBalance(EXISTED_ACCOUNT_ID, "EUR",
                    LocalDate.parse("2022-01-01", DateTimeFormatter.ISO_LOCAL_DATE));
            });

        assertEquals("Integration error occurred", exception.getMessage());
        assertEquals("EX-5000", exception.getErrorCode());
    }

    private void prepareNbpRatesService() {
        try {
            nbpRatesService = mock(NbpRatesService.class);
            when(nbpRatesService.getRate("EUR", "2022-02-11")).thenReturn(mock(NbpRate.class));
            when(nbpRatesService.getRate("EUR", "2022-01-01")).thenThrow(
                new CurrExIntegrationException("Integration error occurred", "EX-5000"));
        } catch (CurrExIntegrationException ex) {

        }

    }

    private void prepareAccountsRepository() {
        accountsRepository = mock(AccountsRepository.class);

        List<Account> accounts = new ArrayList<>();
        Account account = new Account();
        account.setId(EXISTED_ACCOUNT_ID);
        account.setBalance(BigDecimal.valueOf(500.0));
        accounts.add(account);

        account = new Account();
        account.setId(UUID.fromString("6227f4b7-a336-4ea6-a6ec-894e1fefc947"));
        account.setBalance(BigDecimal.valueOf(2500.0));
        accounts.add(account);

        when(accountsRepository.getAccounts()).thenReturn(accounts);
        when(accountsRepository.getById(EXISTED_ACCOUNT_ID)).thenReturn(accounts.get(0));
        when(accountsRepository.getById(NOT_EXISTED_ACCOUNT_ID)).thenThrow(
            new CurrExException("Account not found", "EX-4000"));
    }

    private void prepareConverter() {
        nbpRateConverter = mock(Converter.class);
        ExchangeResult exchangeResult = new ExchangeResult();
        exchangeResult.setRate(4.5163);
        exchangeResult.setCurrencyCode("EUR");
        exchangeResult.setDate(LocalDate.parse("2022-02-11", DateTimeFormatter.ISO_LOCAL_DATE));

        //        when(exchangeResult.getRate()).thenReturn(4.5163);
        //        when(exchangeResult.getCurrencyCode()).thenReturn("EUR");
        //        when(exchangeResult.getDate()).thenReturn(LocalDate.parse("2022-02-11", DateTimeFormatter.ISO_LOCAL_DATE));
        when(nbpRateConverter.convert(any(NbpRate.class))).thenReturn(exchangeResult);
    }

}