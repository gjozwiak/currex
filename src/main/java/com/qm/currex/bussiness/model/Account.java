package com.qm.currex.bussiness.model;

import java.math.BigDecimal;
import java.util.UUID;

public class Account {

    private UUID id;
    private BigDecimal balance;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance.setScale(2);
    }

}
