package com.qm.currex.integration.nbp;

import com.qm.currex.bussiness.model.ExchangeResult;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@Component
public class NbpRateConverter implements Converter<NbpRate, ExchangeResult> {

    @Override
    public ExchangeResult convert(NbpRate source) {
        final ExchangeResult result = new ExchangeResult();
        final Map<String, Object> rate = source.getRates().get(0);
        result.setRate((double) rate.get("mid"));
        result.setCurrencyCode(source.getCode());
        result.setDate(LocalDate.parse((String) rate.get("effectiveDate"), DateTimeFormatter.ISO_LOCAL_DATE));
        return result;
    }

}
