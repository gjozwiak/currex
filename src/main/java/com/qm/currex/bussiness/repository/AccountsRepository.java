package com.qm.currex.bussiness.repository;

import com.qm.currex.bussiness.error.CurrExException;
import com.qm.currex.bussiness.model.Account;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Configuration
@ConfigurationProperties("repository")
public class AccountsRepository {

    private List<Account> accounts = new ArrayList<>();

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public Account getById(final UUID clientId) {
        return accounts.stream().filter(a -> a.getId().equals(clientId)).findFirst()
            .orElseThrow(() -> new CurrExException("Account not found", "EX-4000"));
    }

}
