# CurrEx

Test application for ***QualityMinds***.

### Endpoints:

* [api/v1/accounts](http://localhost:8080/api/v1/accounts)
* [api/v1/exchange/321e7f98-72b7-409e-9a52-24bdcc58d74c](http://localhost:8080/api/v1/exchange/321e7f98-72b7-409e-9a52-24bdcc58d74c)

---

### Run Application:

`mvn -Prun`

### Unit Tests:

`mvn -Put`

### Integration Tests:

`mvn -Pit`