package com.qm.currex.integration;

import com.qm.currex.integration.error.CurrExErrorDecoder;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IntegrationConfiguration {

    @Bean
    public ErrorDecoder errorDecoder() {
        return new CurrExErrorDecoder();
    }
    
}
