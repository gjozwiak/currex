package com.qm.currex.bussiness;

import com.qm.currex.bussiness.error.CurrExException;
import com.qm.currex.bussiness.model.Account;
import com.qm.currex.bussiness.model.ExchangeResult;
import com.qm.currex.bussiness.repository.AccountsRepository;
import com.qm.currex.core.AccountService;
import com.qm.currex.core.ExchangeService;
import com.qm.currex.integration.error.CurrExIntegrationException;
import com.qm.currex.integration.nbp.NbpRate;
import com.qm.currex.integration.nbp.NbpRatesService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

@Service
public class AccountManager implements AccountService, ExchangeService {

    private NbpRatesService nbpRatesService;
    private AccountsRepository accountsRepository;
    private Converter<NbpRate, ExchangeResult> nbpRateConverter;

    public AccountManager(NbpRatesService nbpRatesService,
                          AccountsRepository accountsRepository,
                          Converter<NbpRate, ExchangeResult> nbpRateConverter) {
        this.nbpRatesService = nbpRatesService;
        this.accountsRepository = accountsRepository;
        this.nbpRateConverter = nbpRateConverter;
    }

    @Override
    public ExchangeResult foreignCurrencyAccountBalance(UUID clientId, String currencyCode, LocalDate date) {
        try {
            final Account account = accountsRepository.getById(clientId);
            final ExchangeResult exchangeResult = nbpRateConverter.convert(
                nbpRatesService.getRate(currencyCode, date.format(DateTimeFormatter.ISO_LOCAL_DATE)));
            exchangeResult.setMoneyValue(
                account.getBalance().divide(new BigDecimal(exchangeResult.getRate()), 2, RoundingMode.HALF_EVEN));
            return exchangeResult;
        } catch (CurrExIntegrationException ex) {
            throw new CurrExException(ex.getMessage(), ex.getErrorCode());
        }
    }

    @Override
    public List<Account> getAccounts() {
        return accountsRepository.getAccounts();
    }

}
